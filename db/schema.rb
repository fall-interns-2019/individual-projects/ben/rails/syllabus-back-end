# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# # database schema. If you need to create the application database on another
# # system, you should be using db:schema:load, not running all the migrations
# # from scratch. The latter is a flawed and unsustainable approach (the more migrations
# # you'll amass, the slower it'll run and the greater likelihood for issues).
# #
# # It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_19_212411) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.bigint "course_id"
    t.string "name"
    t.string "due_date"
    t.string "description"
    t.string "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_assignments_on_course_id"
  end

  create_table "courses", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.string "number"
    t.string "section"
    t.string "term"
    t.string "instructor"
    t.string "syllabus"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_courses_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "assignments", "courses"
  add_foreign_key "courses", "users"
end
