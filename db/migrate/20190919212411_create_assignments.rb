class CreateAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :assignments do |t|
      t.references :course, foreign_key: true
      t.string :name
      t.string :due_date
      t.string :description
      t.string :points

      t.timestamps
    end
  end
end
