class User < ApplicationRecord

  validates :email, uniqueness: true

  has_many :courses
  has_many :assignments, through: :courses
end
