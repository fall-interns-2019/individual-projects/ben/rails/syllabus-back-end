class Assignment < ApplicationRecord
  belongs_to :course
  has_one :user, through: :course
end
