class Api::V1::UsersController < ApplicationController
  def index
    users = User.all
    render json: users
  end

  def show
    render json: User.find(params[:id])
  end

  def new
    User.new
  end

  def create
    user = User.find_or_create_by(user_params)
    render json: user
  end

  def update
    user = User.find(params[:id])
    if user.nil?
      render json: 'user not found', status: :not_found
    elsif user.update(user_params)
      render json: user
    else
      render json: user.errors, status: :bad_request
    end
  end

  def destroy
    user.destroy
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :first_name, :last_name)
  end
end
