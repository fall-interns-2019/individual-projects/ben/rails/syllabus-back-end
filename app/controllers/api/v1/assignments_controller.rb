class Api::V1::AssignmentsController < ApplicationController
  def index
    assignments = Assignment.all
    render json: assignments
  end

  def show
    assignment = Assignment.find_by(id: params[:id])
    render json: assignment
  end

  def new
    Assignment.new
  end

  def create
    assignment = Assignment.find_or_create_by(assignment_params)
    render json: assignment
  end

  def destroy
    assignment = Assignment.find_by(id: params[:id])
    assignment.destroy
  end

  private

  def assignment_params
    params.require(:assignment).permit(:course_id, :name, :due_date, :description, :points)
  end
end
