class Api::V1::CoursesController < ApplicationController
  def index
    courses = Course.all
    render json: courses
  end

  def show
    course= Course.find_by(id: params[:id])
    render json: course
  end

  def new
    Course.new
  end

  def create
    course = Course.find_or_create_by(course_params)
    render json: course
  end

  def update
    course = Course.find(params[:id])
    if course.nil?
      render json: 'course not found', status: :not_found
    elsif course.update(course_params)
      render json: course
    else
      render json: course.errors, status: :bad_request
    end
  end

  def destroy
    course = Course.find_by(id: params[:id])
    course.destroy
  end

  private

  def course_params
    params.require(:course).permit(:user_id, :name, :number, :section, :term, :instructor, :syllabus)
  end
end
