class CourseSerializer < ActiveModel::Serializer
  belongs_to :user
  has_many :assignments, dependent: :destroy
  attributes :id, :user_id, :name, :number, :section, :term, :instructor, :syllabus
end
