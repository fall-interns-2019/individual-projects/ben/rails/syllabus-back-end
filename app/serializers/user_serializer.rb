class UserSerializer < ActiveModel::Serializer
  has_many :courses
  has_many :assignments, through: :courses
  attributes :id, :email, :password, :first_name, :last_name
end
