class AssignmentSerializer < ActiveModel::Serializer
  belongs_to :course
  has_one :user, through: :course
  attributes :id, :course_id, :name, :due_date, :description, :points
end
